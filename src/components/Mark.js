import React from "react";


export default function Mark() {
  return (
    <div className="marka">
      <div className="mark">
        <div className="c-mark">
          <img className="img-mark" src="./mark.png" alt="mark" />
        </div>
        <div className="text-mark">
          <p>2021 RXdeliverdnow.</p>
        </div>
      </div>
    </div>
  );
}

import React from 'react'
import Input from './Input'


export default function Password() {
  return (
    <>
        <div className='com-chil hid'>
            <div className='icon'>
                <img src='./lock.svg' alt='lock' />
            </div>
            <Input type='password' name='Password' cname='f-input test' />
            <div className='icon1'>
                <img src='./hidden.svg' alt='hidden' />
            </div>
        </div>
        <div className='com-chil hid'>
            <div className='icon'>
                <img src='./lock.svg' alt='lock' />
            </div>
            <Input type='password' name='Confirm Password' cname='f-input test'/>
            <div className='icon1'>
                <img src='./hidden.svg' alt='hidden' />
            </div>
        </div>
    </>
  )
}

import React from 'react'
import Input from './Input'


export default function Contact() {
  return (
    <>
        <div className='com-chil'>
            <div className='icon'>
                <img src='./Email.svg' alt='email'/>
            </div>
            <Input type='text' name='Email' cname='f-input'/> 
        </div>
        <div className='com-chil'>
            <div className='icon'>
                <img src='./Email.svg' alt='email' />
            </div>
            
            <Input type='text' name='First Name' cname='f-input'/>
        </div>
        <div className='com-chil'>
            <div className='icon'>
                <img src='./name.svg' alt='name' />
            </div>
            
            <Input type='text' name='Last Name' cname='f-input'/>
        </div>
        <div className='com-chil'>
            <div className='icon'>
                <img src='./Phone.svg' alt='phone' />
            </div>
            <Input type='text' name='Phone Number' cname='f-input'/>
        </div>
    </>
  )
}

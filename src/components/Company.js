import React from 'react'
import Input from './Input'
import Textarea from './Textarea'


export default function Company() {
  return (
        <>
            <div className='com-chil'>
                <div className='icon'>
                  <img src='./Email.svg' alt='email' />
                </div>
                <Input type='text' name='Company Name' cname='f-input'/> 
            </div>
            <div className='com-chil  text'>
                <div className='icon'>
                  <img src='./map.svg' alt='map'/>
                </div>
                <Textarea type='text' name='Company Address' cname='f-inputa textaa'/>
            </div>
        </>
    )
}

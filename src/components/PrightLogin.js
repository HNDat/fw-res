import React from 'react'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import "./PrightLogin.css"


function PrightLogin() { 
    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
      };
    return (
        <>
            <div className='block-tracking'>
                <Slider {...settings}>
                    <div className='bao'>
                        <div className='tracking'>
                            <img src='./Img/img3.svg' alt='a'/>
                        </div>
                        <div className='text-tracking'>
                            <div className='owner'>
                                <p>Schedule & Optimize Delivery</p>
                            </div>
                            <div className='tt'>
                                <p>Schedule all your deliveries in one platform</p>
                            </div>
                        </div>
                    </div>
                    <div className='bao'>
                        <div className='tracking'>
                            <img src='./Img/img2.svg' alt='a'/>
                        </div>
                        <div className='text-tracking'>
                            <div className='owner'>
                                <p>Proof of Delivery</p>
                            </div>
                            <div className='tt'>
                                <p>Get the proof you need to meet compliance (signature, photo, and geotags).</p>
                            </div>
                        </div>
                    </div>
                    <div className='bao'>
                        <div className='tracking'>
                            <img src='./Img/img1.svg' alt='a'/>
                        </div>
                        <div className='text-tracking'>
                            <div className='owner'>
                                <p>Tracking</p>
                            </div>
                            <div className='tt'>
                                <p>Track postal, on-demand, scheduled and/or in-house drivers all in one place.</p>
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>
        </>
    )
}

export default PrightLogin;


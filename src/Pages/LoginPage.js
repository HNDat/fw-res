import React from "react";
import PrightLogin from "../components/PrightLogin";
import Mark from "../components/Mark";
import Input from "../components/Input";
import { Link } from "react-router-dom";
import "./LoginPage.css";

export default function LoginPage() {
  return (
    <div>
      <div className="logo1">
        <img src="./logo-delivered-now.svg" className="img-logo" alt="logo" />
      </div>
      <div className="container-fluid">
        <div className="row">
          <div className="page">
            <div className="col">
              <div className="page-left">
                <div className="page-left-in">
                  <div className="logo">
                    <img
                      src="./logo-delivered-now.svg"
                      className="img-logo"
                      alt="logo"
                    />
                  </div>

                  <div className="com-name marginEmail">
                    <div className="com-chil">
                      <div className="icon">
                        <img src="./Email.svg" alt="email" />
                      </div>
                      <Input type="text" name="Email" cname="f-input" />
                    </div>

                    <div className="com-chil hid">
                      <div className="icon">
                        <img src="./lock.svg" alt="lock" />
                      </div>
                      <Input
                        type="password"
                        name="Password"
                        cname="f-input test"
                      />
                      <div className="icon1">
                        <img src="./hidden.svg" alt="hidden" />
                      </div>
                    </div>
                  </div>

                  <div className="but-reg">
                    <button className="button">LOGIN</button>
                  </div>

                  <Link to="/register" className="linkpage">
                    <div className="login-now">
                      {/* <p className="text">I have an account. </p> */}
                      <p className="login">Register</p>
                    </div>
                  </Link>

                  <div className="login-now marginLogin">
                    <p className="login">I forgot password</p>
                  </div>

                  <Mark />
                  <div className="marka1">
                    <div className="mark">
                      <div className="c-mark">
                        <img className="img-mark" src="./mark.png" alt="mark" />
                      </div>
                      <div className="text-mark">
                        <p>2021 RXdeliverdnow.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="page-right">
                <div className="fa hei">
                  <PrightLogin />
                  {/* <Slider/> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

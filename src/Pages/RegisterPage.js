import React from "react";
import Pright from "../components/Pright";
import Mark from "../components/Mark";
import Password from "../components/Password";
import Contact from "../components/Contact";
import Company from "../components/Company";
import { Link } from "react-router-dom";

export default function RegisterPage() {
  return (
    <div>
      <div className="logo1">
        <img src="./logo-delivered-now.svg" className="img-logo" alt="logo" />
      </div>
      <div className="container-fluid">
        <div className="row">
          <div className="page">
            <div className="col">
              <div className="page-left">
                <div className="page-left-in">
                  <div className="logo">
                    <img
                      src="./logo-delivered-now.svg"
                      className="img-logo"
                      alt="logo"
                    />
                  </div>

                  <div className="res">
                    <h2>REGISTER</h2>
                  </div>

                  <div className="com-name">
                    <Company />
                  </div>

                  <div className="owner">
                    <p>Owner</p>
                  </div>

                  <div className="com-name">
                    <Contact />
                    <Password />
                  </div>

                  <div className="but-reg">
                    <button className="button">REGISTER</button>
                  </div>

                  <div className="login-now">
                    <p className="text">I have an account. </p>
                    <Link to="/" className="linkpage">
                      <p className="login">Login Now.</p>
                    </Link>
                  </div>

                  <Mark />
                  <div className="marka1">
                    <div className="mark">
                      <div className="c-mark">
                        <img className="img-mark" src="./mark.png" alt="mark" />
                      </div>
                      <div className="text-mark">
                        <p>2021 RXdeliverdnow.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="page-right">
                <div className="fa">
                  <Pright />
                  {/* <Slider/> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
